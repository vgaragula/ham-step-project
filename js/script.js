
$(document).ready(function (event) {
$(function(){
 
      $('.slider-nav').slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        arrows:true,
        asNavFor: '.reviews-wrapper',
        dots: false,
     variableWidth:true,
        centerMode: true,
        focusOnSelect: true,
        
        
      });
      $('.reviews-wrapper').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
      arrows:false,
        fade: true,
        asNavFor: '.slider-nav'
      });
     
})


    $(function () {
        //scroll to top
        $('.btn-scroll-to-top').click(function () {
            $('html, body').animate({ scrollTop: 0 }, 500);
            return false;
        });

        $(document).scroll(function () {
            let y = $(this).scrollTop();
            if (y > 800) {
                $('.btn-scroll-to-top').addClass('hidden');
            } else {
                $('.btn-scroll-to-top').removeClass('hidden');
            }
        });
        $(document).on('click', "#listWorkWrapperLoadBtn", function () {
            $("#loader").show(2000, function () {
                $("#loader").hide(function () {
                    $("#listWorkWrapperLoad").slideDown("slow")
                    $("#listWorkWrapperLoadBtn").hide("200")
                });
            })

        })
        $(document).on('click', "#loadBtn", function () {
            $("#loader").show(2000, function () {
                $("#loader").hide();
            })

        })
    }
    )



    
})


displayBanner()

document.getElementById('buttonCookies').addEventListener('click', () => {
    document.querySelector('#cookiesBanner').classList.add('banner-hidden')
    localStorage.setItem('cookiesBanner', 'off')
})

function displayBanner() {
    let storageCookiesBanner = localStorage.getItem('cookiesBanner');
    if (storageCookiesBanner) {
        document.querySelector('#cookiesBanner').classList.add('banner-hidden')
    }
}
filterCard()
function filterCard() {
    let ul = document.querySelector(".filter-btn-list");
    ul.addEventListener("click", function (ev) {
        if (ev.target.tagName !== 'BUTTON') return false
        let data = ev.target.dataset['type'];
        const arrImg = document.querySelectorAll(".active-img")
        arrImg.forEach(
            elem => {
                elem.classList.remove("active-img");
            })
        document.querySelector(".filter-btn-active").classList.remove("filter-btn-active");
        const arrImgShow = document.querySelectorAll(`[data-li = ${data}]`)
        arrImgShow.forEach(
            elem => {
                elem.classList.add("active-img");
            }
        )
        if (data == 'tab1') {
            const arrImg = document.querySelectorAll(".work-img")
            arrImg.forEach(
                elem => {
                    elem.classList.add("active-img");
                })
        }


        ev.target.classList.add("filter-btn-active")
    })
}

ourServicesTabs()
function ourServicesTabs() {
    let ul = document.querySelector(".tabs-list")
    ul.addEventListener("click", function (ev) {
        if (ev.target.tagName !== 'LI') return false
        let data = ev.target.dataset['type'];

        document.querySelector(".tabs-content-block-active").classList.remove("tabs-content-block-active")


        let item = document.querySelector(".tabs-item-active")
        item.classList.remove("tabs-item-active")
        item.classList.remove("tabs-item-active-triangle")
        document.querySelector(`[data-li = ${data}]`).classList.add("tabs-content-block-active")

        ev.target.classList.add("tabs-item-active")
        ev.target.classList.add("tabs-item-active-triangle")
    })
}

